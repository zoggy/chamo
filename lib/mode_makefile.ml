(*********************************************************************************)
(*                Chamo                                                          *)
(*                                                                               *)
(*    Copyright (C) 2003-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Makefile sourceview mode. *)

let _ = Mode_makefile_rc.read ()
let _ = Mode_makefile_rc.write ()

let mode_name = Mode_makefile_rc.mode_name

let coms = [
]

let _ = List.iter
    (fun (name, args, more, f) ->
      Sourceview.register_com
        ~prefix: mode_name name args ?more f)
    coms

class mode =
  object
    inherit Sourceview.empty_mode
    method name = mode_name
    method key_bindings : (Stk.Key.keystate list * string) list =
      Ocf.get Mode_makefile_rc.key_bindings
    method menus : (string * Stk.Menu.menu_entry list) list = []
  end

let mode = new mode
let _ = Sourceview.register_mode mode