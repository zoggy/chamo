(*********************************************************************************)
(*                Chamo                                                          *)
(*                                                                               *)
(*    Copyright (C) 2003-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

let mode_name = "ocaml"
let includes_global_var = mode_name^"_includes"

let rc_file = Sourceview_rc.mode_rc_file mode_name
let local_rc_file = Sourceview_rc.local_mode_rc_file mode_name

let group = ref Ocf.group
let add_to_group path o = group := Ocf.add !group path o
let group () = !group

let x = 1 - 1
let default_key_bindings  =
  let module K = Tsdl.Sdl.K in
  let module Kmod = Tsdl.Sdl.Kmod in
  Rc.to_keystates [
    [0, K.tab], mode_name^"_indent_line" ;
    [Kmod.ctrl, K.x ; Kmod.ctrl, K.tab], mode_name^"_indent_buffer";
    [Kmod.ctrl, K.x ; Kmod.ctrl, K.a], mode_name^"_switch_file";
    [Kmod.ctrl, K.o ; Kmod.ctrl, K.c], mode_name^"_build";
    [Kmod.alt, K.t], mode_name^"_display_type_annot";
    [Kmod.(ctrl + alt), K.t], mode_name^"_copy_type_annot";
    [Kmod.alt, K.c], mode_name^"_display_call_annot";
    [Kmod.alt, K.i], mode_name^"_display_ident_annot";
    [Kmod.alt, K.j], mode_name^"_jump_to_local_def";
    [Kmod.(ctrl + alt), K.c], mode_name^"_show_stack_calls";
    [Kmod.(ctrl + alt), K.x], mode_name^"_expand_ext_idents";
]

let key_bindings =
  let o = Ocf.list Config.binding_wrappers
    ~doc:"Key bindings" default_key_bindings
  in
  add_to_group ["key_bindings"] o;
  o

let stack_call_bgcolor =
  let o = Ocf.option ~doc:"Background color of non-tail calls"
    Stk.Color.ocf_wrapper Stk.Color.red
  in
  add_to_group ["stack_call_colors";"background"] o;
  o

let stack_call_fgcolor =
  let o = Ocf.option ~doc:"Foreground color of non-tail calls"
    Stk.Color.ocf_wrapper Stk.Color.yellow
  in
  add_to_group ["stack_call_colors";"foreground"] o;
  o


let read () = Ocf.from_file (group()) rc_file
let write () = Ocf.to_file (group()) rc_file

let (add_sourceview_mode_ocaml_key_binding,
   add_sourceview_mode_ocaml_key_binding_string) =
  Sourceview_rc.create_add_sourceview_mode_binding_commands
    key_bindings mode_name;;

let local_group = ref Ocf.group
let add_to_local_group path o = local_group := Ocf.add !local_group path o
let local_group () = !local_group

let ocamlbuild_commands =
  let o = Ocf.list Ocf.Wrapper.(pair string string)
    ~doc:"ocamlbuild commands associated to edited files"
    []
  in
  add_to_local_group ["ocamlbuild_commands"] o;
  o

let local_read () = Ocf.from_file (local_group()) local_rc_file;;
let local_write () = Ocf.to_file (local_group()) local_rc_file
;;

 