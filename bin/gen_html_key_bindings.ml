
let p b = Printf.bprintf b

let print_ks b l =
  List.iter (fun k -> p b "<keys>%s</keys> " (Stk.Key.string_of_keystate k)) l

let gen_table =
  let f b (ks, command) =
    p b "<tr><td><keys>";
    List.iter (fun k -> p b "%s " (Stk.Key.string_of_keystate k)) ks ;
    p b "</keys></td><td><com>%s</com></td></tr>\n" command;
  in
  fun b ~title ~id l ->
    p b "<table id=%S>\n" id;
    p b "<tr><th colspan=\"2\" class=\"title\">%s</th></tr>\n" title;
    List.iter (f b) l;
    p b "</table>"

let gen_tables b l =
  p b "<div class=\"bindings\">\n";
  List.iter
    (fun (title, id, bindings) ->
       gen_table b ~title ~id bindings;
    ) l;
  p b "</div>\n"

let gen_groups b l =
  p b "<div class=\"binding-group\">\n";
  List.iter (gen_tables b) l ;
  p b "</div>\n"

let groups =
  [
    [ "Window", "gui",
      Ocf.get Chamo.Gui_rc.window_key_bindings ;

      "Minibuffer", "minibuffer",
      Ocf.get Chamo.Minibuffer_rc.key_bindings ;

      "Multiclip", "multiclip",
      Ocf.get Chamo.Multiclip_rc.key_bindings ;
    ] ;

    [ "Sourceview", "sourceview",
      Ocf.get Chamo.Sourceview_rc.key_bindings ;
    ] ;

    [
        "OCaml mode", "ocaml",
        Ocf.get Chamo.Mode_ocaml_rc.key_bindings ;

        "Changelog mode", "changelog",
        Ocf.get Chamo.Mode_changelog_rc.key_bindings ;

        "Makefile mode", "makefile",
        Ocf.get Chamo.Mode_makefile_rc.key_bindings ;
    ]
  ]

let b = Buffer.create 256
let () = gen_groups b groups
let () = print_endline (Buffer.contents b)

  