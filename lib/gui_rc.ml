(*********************************************************************************)
(*                Chamo                                                          *)
(*                                                                               *)
(*    Copyright (C) 2003-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Tsdl
open Stk.Key

let rc_file = Config.rc_file "gui"

let group = ref Ocf.group
let add_to_group path o = group := Ocf.add !group path o
let add_group_to_group path g = group := Ocf.add_group !group path g
let group () = !group

let default_minibuffer_history_size = 50

let default_abort_binding = Stk.Key.keystate ~mods:Sdl.Kmod.ctrl Sdl.K.g

let default_window_key_bindings  =
  Rc.to_keystates [
      [Sdl.Kmod.ctrl, Sdl.K.x; Sdl.Kmod.ctrl,Sdl.K.c], "close_active_window" ;
      [Sdl.Kmod.ctrl, Sdl.K.n], "new_window" ;
      [Sdl.Kmod.ctrl, Sdl.K.x ; Sdl.Kmod.ctrl, Sdl.K.s], "save_active_view" ;
      [Sdl.Kmod.ctrl, Sdl.K.x ; Sdl.Kmod.ctrl, Sdl.K.w], "save_active_view_as" ;
      [Sdl.Kmod.ctrl, Sdl.K.x ; Sdl.Kmod.ctrl, Sdl.K.f], "open_file" ;
      [Sdl.Kmod.ctrl, Sdl.K.b], "cycle_view" ;
      [Sdl.Kmod.ctrl, Sdl.K.tab], "cycle_tab" ;
      [Sdl.Kmod.(ctrl+shift), Sdl.K.tab], "rev_cycle_tab" ;
      [Sdl.Kmod.ctrl, Sdl.K.x; Sdl.Kmod.ctrl, Sdl.K.t], "new_tab" ;
      [Sdl.Kmod.ctrl, Sdl.K.v], "split_vertically" ;
      [Sdl.Kmod.ctrl, Sdl.K.x; Sdl.Kmod.shift, Sdl.K.k3], "split_vertically" ;
      [Sdl.Kmod.ctrl, Sdl.K.h], "split_horizontally" ;
      [Sdl.Kmod.ctrl, Sdl.K.x; Sdl.Kmod.shift, Sdl.K.k2], "split_horizontally" ;
      [Sdl.Kmod.ctrl, Sdl.K.x ; Sdl.Kmod.shift, Sdl.K.k0], "destroy_active_view" ;
      [Sdl.Kmod.ctrl, Sdl.K.x ; Sdl.Kmod.ctrl, Sdl.K.x;  0, Sdl.K.l], "print_key_bindings" ;
      [Sdl.Kmod.ctrl, Sdl.K.x ; 0, Sdl.K.v], "popup_pick_hidden_view" ;
      [Sdl.Kmod.ctrl, Sdl.K.x ; 0, Sdl.K.l], "pop_last_hidden_view" ;
      [Sdl.Kmod.ctrl, Sdl.K.y], "paste" ;
      [Sdl.Kmod.ctrl, Sdl.K.c], "copy" ;
      [Sdl.Kmod.ctrl, Sdl.K.w], "cut" ;
      [Sdl.Kmod.lalt, Sdl.K.x], "prompt_command" ;
    ]

let minibuffer_history_size =
  let o = Ocf.int ~doc: "The size of histories in minibuffer"
      default_minibuffer_history_size
  in
  add_to_group ["minibuffer_history_size"] o ;
  o

let abort_binding =
  let o = Ocf.option Stk.Key.keystate_ocf_wrapper
    ~doc:"The key combination to use to reset the key stroke state"
    default_abort_binding
  in
  add_to_group ["abort_binding"] o;
  o

let window_key_bindings =
  let o = Ocf.list Config.binding_wrappers
    ~doc: "Common key bindings for windows"
    default_window_key_bindings
  in
  add_to_group ["window_key_bindings"] o;
  o

let () = add_group_to_group ["log"] (Log.group())

let read () = Ocf.from_file (group()) rc_file
let write () = Ocf.to_file (group()) rc_file

let _ = read () ; write ()


