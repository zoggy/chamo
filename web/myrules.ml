
let re = Str.regexp "[ \n\t]+"
let f stog env ?loc atts subs =
  let module Xr = Xtmpl.Rewrite in
  let nbsp = Xr.node ("","nbsp") [] in
  let xmls =
    match Xr.merge_cdata_list subs with
    | [D { Xtmpl.Types.text } ] ->
        let l = Str.split_delim re text in
        let rec iter acc = function
        | [] -> List.rev acc
        | "" :: q -> iter acc q
        | s :: q ->
            let atts = Xr.atts_one ("","class") [Xr.cdata "keys"] in
            let t = Xr.node ("","span") ~atts [Xr.cdata s] in
            let acc = match acc with [] -> [t] | l -> t :: nbsp :: acc in
            iter acc q
        in
        iter [] l
    | _ -> [%xtmpl.string {| <error_>&lt;keys&gt; should contains cdata and only cdata</error_> |} ] ()
  in
  (stog, xmls)

let () = Stog.Plug.register_html_base_rule ("","keys") f
