(*********************************************************************************)
(*                Chamo                                                          *)
(*                                                                               *)
(*    Copyright (C) 2003-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

type 'a bookmarks = {
  bk_table : (string, 'a) Hashtbl.t;
  bk_cf_group : [`Open] Ocf.group ;
  bk_cf_o : (string * 'a) list Ocf.conf_option ;
  }
;;

let create_from_ocf_wrappers ?doc wrappers =
  let o = Ocf.list ?doc Ocf.Wrapper.(pair string wrappers) [] in
  let g = Ocf.add Ocf.group ["bookmarks"] o in
  {
    bk_table = Hashtbl.create 101;
    bk_cf_group = g ;
    bk_cf_o = o ;
  }
;;


let create ?doc data_of_string string_of_data =
  let w = Ocf.Wrapper.string_ string_of_data data_of_string in
  create_from_ocf_wrappers ?doc w
;;

let get bk name = Hashtbl.find bk.bk_table name;;
let set bk name data = Hashtbl.replace bk.bk_table name data;;
let remove bk name = Hashtbl.remove bk.bk_table name;;

let list bk =
  Hashtbl.fold (fun name data acc -> (name, data) :: acc) bk.bk_table []
;;

let store bk file =
  let l = list bk in
  match l with
  | [] -> (try Unix.unlink file with _ -> ())
  | _ ->
      Ocf.set bk.bk_cf_o l;
      Ocf.to_file bk.bk_cf_group file
;;

let load bk file =
  Hashtbl.clear bk.bk_table;
  Ocf.from_file bk.bk_cf_group file;
  let f (name, data) = set bk name data in
  List.iter f (Ocf.get bk.bk_cf_o)
;;

