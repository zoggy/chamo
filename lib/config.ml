(*********************************************************************************)
(*                Chamo                                                          *)
(*                                                                               *)
(*    Copyright (C) 2003-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Some configuration stuff of the editor. *)

let local_dir_rc_file_prefix = ".chamo."
let local_dir_rc_file name = ".chamo." ^ name

let getenv var =
  match Sys.getenv var with
  | exception Not_found -> None
  | "" -> None
  | v -> Some v

let home =
  try Sys.getenv "HOME"
  with Not_found -> ""

let login =
  try Sys.getenv "USER"
  with Not_found -> Filename.basename home

let rc_dir =
  let xdg_config_home =
    match getenv "XDG_CONFIG_HOME" with
    | Some d -> d
    | None -> Filename.concat home ".config"
  in
  let d = Filename.concat xdg_config_home "chamo" in
  let exist =
    try (Unix.stat d).Unix.st_kind = Unix.S_DIR
    with _ -> false
  in
  if not exist then
    begin
      let com = Printf.sprintf "mkdir -p %s" (Filename.quote d) in
      (match Sys.command com with
       | 0 -> ()
       | _ ->
         prerr_endline
             (Printf.sprintf "Could not create configuration directory %s" d)
      )
    end;
  d

let rc_file s = Filename.concat rc_dir ("chamo."^s)

let keystates_wrappers = Ocf.Wrapper.list Stk.Key.keystate_ocf_wrapper
let binding_wrappers =
  Ocf.Wrapper.(pair keystates_wrappers string)

let ocamlfind = ref "ocamlfind"
let set_ocamlfind s = ocamlfind := s
let ocamlfind () = !ocamlfind
