(*********************************************************************************)
(*                Chamo                                                          *)
(*                                                                               *)
(*    Copyright (C) 2003-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Configuration file of the ocaml mode. *)

(** The mode name. *)
val mode_name : string

(** Name of the global variable where includes are stored. These includes
     can be used to launch commands (compilation, analysis, ...) of
     edited ocaml files. *)
val includes_global_var : string

(** The user's configuration file. *)
val rc_file : string

(** The local configuration file. *)
val local_rc_file : string

(** The option for the key bindings in the mode. *)
val key_bindings : (Stk.Key.keystate list * string) list Ocf.conf_option

(** The background color used when showing non-tail calls. *)
val stack_call_bgcolor : Stk.Color.t Ocf.conf_option

(** The foreground color used when showing non-tail calls. *)
val stack_call_fgcolor : Stk.Color.t Ocf.conf_option

(** Read the configuration file. *)
val read : unit -> unit

(** Write the configuration file. *)
val write : unit -> unit

(** Same as {!Sourceview_rc.add_sourceview_key_binding} and
     {!Sourceview_rc.add_sourceview_key_binding_string} but for the key bindings
     of the mode. *)
val add_sourceview_mode_ocaml_key_binding :
  Stk.Key.keystate list -> string -> unit
val add_sourceview_mode_ocaml_key_binding_string :
  string -> string -> unit

(** OCamlbuild commands associated to files. *)
val ocamlbuild_commands : (string * string) list Ocf.conf_option

(** Read the local configuration file. *)
val local_read : unit -> unit

(** Write the local configuration file. *)
val local_write : unit -> unit

