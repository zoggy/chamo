(*********************************************************************************)
(*                Chamo                                                          *)
(*                                                                               *)
(*    Copyright (C) 2003-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

let factory_name = "multiclip"
let rc_file = Config.rc_file factory_name

let group = ref Ocf.group
let add_to_group path o = group := Ocf.add !group path o
let group () = !group

let default_key_bindings = Rc.to_keystates Tsdl.Sdl.[
    [ Kmod.ctrl, K.m ; Kmod.ctrl, K.c], factory_name^"_copy_selection" ;
    [ Kmod.ctrl, K.m ; Kmod.lalt, K.c], factory_name^"_copy" ;
    [ Kmod.ctrl, K.m ; Kmod.ctrl, K.x], factory_name^"_remove" ;
];;

let key_bindings =
  let o = Ocf.list Config.binding_wrappers
    ~doc: "Key bindings" default_key_bindings
  in
  add_to_group ["key_bindings"] o;
  o

;;

let read () = Ocf.from_file (group()) rc_file;;
let write () = Ocf.to_file (group()) rc_file;;

let (add_multiclip_key_binding, add_multiclip_key_binding_string) =
  Commands.create_add_binding_commands key_bindings factory_name