(*********************************************************************************)
(*                Chamo                                                          *)
(*                                                                               *)
(*    Copyright (C) 2003-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

(** Handling the configuration file for windows. *)

(** The user's configuration file. *)
val rc_file : string

(** Ocf option group. *)
val group : unit -> [`Open] Ocf.group

(** The option for the size of histories in minibuffers. *)
val minibuffer_history_size : int Ocf.conf_option

(** The "abort" key binding. This is the binding use to abort
     the current key press sequence. Default is C-g. *)
val abort_binding : Stk.Key.keystate Ocf.conf_option

(** The key bindings for the chamo windows. These bindings are always
     accessible whatever the view which has the focus. *)
val window_key_bindings : (Stk.Key.keystate list * string) list Ocf.conf_option

(** Read the configuration file. *)
val read : unit -> unit

(** Write the configuration file. *)
val write : unit -> unit
