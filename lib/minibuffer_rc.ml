(*********************************************************************************)
(*                Chamo                                                          *)
(*                                                                               *)
(*    Copyright (C) 2003-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

module K = Tsdl.Sdl.K
module Kmod = Tsdl.Sdl.Kmod
open Stk

let base_name = "minibuffer"
let rc_file = Config.rc_file base_name
let mode_rc_file = Config.rc_file base_name

let group = ref Ocf.group
let add_to_group path o = group := Ocf.add !group path o
let group () = !group

let default_key_bindings  = [
    [Key.keystate K.return], base_name ^ "_eval" ;
    [Key.keystate ~mods:Kmod.alt K.return], base_name ^ "_insert \"\n\"" ;
    [Key.keystate K.tab], base_name ^ "_complete";
  ]
;;
let default_history_key_bindings = [
  [Key.keystate K.up], base_name ^ "_history_previous" ;
  [Key.keystate K.down], base_name ^ "_history_next" ;
  ]
;;
let default_exiting_keys = [
    [Key.keystate K.up];
    [Key.keystate K.down];
    [Key.keystate K.escape];
  ]
;;
let key_bindings =
  let o = Ocf.list Config.binding_wrappers
    ~doc:"Key bindings" default_key_bindings
  in
  add_to_group ["key_bindings"] o;
  o

let history_key_bindings =
  let o = Ocf.list Config.binding_wrappers
    ~doc: "History key bindings" default_history_key_bindings
  in
  add_to_group ["history_key_bindings"] o;
  o

let exiting_keys =
  let o = Ocf.list Config.keystates_wrappers
    ~doc:"Combinations of keys to exit from the minibuffer"
      default_exiting_keys
  in
  add_to_group ["exiting_keys"] o;
  o

let read () = Ocf.from_file (group()) rc_file
let write () = Ocf.to_file (group()) rc_file

let (add_minibuffer_key_binding, add_minibuffer_key_binding_string) =
  Commands.create_add_binding_commands key_bindings base_name

