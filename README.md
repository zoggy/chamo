Chamo is a source code editor in OCaml, using [OCaml-stk](https://zoggy.frama.io/ocaml-stk/).
Users can configure and extend chamo with ocaml code.
More information on https://zoggy.frama.io/chamo/ .
