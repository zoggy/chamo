(*********************************************************************************)
(*                Chamo                                                          *)
(*                                                                               *)
(*    Copyright (C) 2003-2021 Institut National de Recherche en Informatique     *)
(*    et en Automatique. All rights reserved.                                    *)
(*                                                                               *)
(*    This program is free software; you can redistribute it and/or modify       *)
(*    it under the terms of the GNU Lesser General Public License version        *)
(*    3 as published by the Free Software Foundation.                            *)
(*                                                                               *)
(*    This program is distributed in the hope that it will be useful,            *)
(*    but WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              *)
(*    GNU General Public License for more details.                               *)
(*                                                                               *)
(*    You should have received a copy of the GNU General Public License          *)
(*    along with this program; if not, write to the Free Software                *)
(*    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA                   *)
(*    02111-1307  USA                                                            *)
(*                                                                               *)
(*    Contact: Maxence.Guesdon@inria.fr                                          *)
(*                                                                               *)
(*********************************************************************************)

open Stk

class main ?x ?y ?(w=600) ?(h=500) () =
  let toplevel = App.create_window ~resizable:true ?x ?y ~w ~h "Chamo" in
  let evbox = Stk.Event_box.event_box ~pack:toplevel#set_child () in
  let vbox : Box.box = Box.vbox ~pack:evbox#set_child () in
  let hboxmenus : Box.box = Box.hbox ~pack:(vbox#pack ~vexpand:0) () in
  let menubar = Menu.menubar ~pack:(hboxmenus#pack ~hexpand:0) () in
  let (item_file,_) = Menu.label_menuitem ~text:"File"
    ~pack:(menubar#add_item) () in
  let menu_file = Menu.menu ~pack:item_file#set_menu () in
  let (item_new_window,_) = Menu.label_menuitem ~text:"New window" ~pack:menu_file#add_item () in
  let (item_open,_) = Menu.label_menuitem ~text:"Open" ~pack:menu_file#add_item () in
  let (item_save,_) = Menu.label_menuitem ~text:"Save" ~pack:menu_file#add_item () in
  let (item_save_as,_) = Menu.label_menuitem ~text:"Save as ..." ~pack:menu_file#add_item () in
  let (item_reload,_) = Menu.label_menuitem ~text:"Reload" ~pack:menu_file#add_item () in
  let (item_prefs,_) = Menu.label_menuitem ~text:"Preferences" ~pack:menu_file#add_item () in
  let (item_log_window,_) = Menu.label_menuitem ~text:"Log window" ~pack:menu_file#add_item () in
  let (item_close,_) = Menu.label_menuitem ~text:"Close" ~pack:menu_file#add_item () in
  let (item_edit,_) = Menu.label_menuitem ~text:"Edit" ~pack:menubar#add_item () in

  let (item_view,_) = Menu.label_menuitem ~text:"View" ~pack:menubar#add_item () in
  let menu_view = Menu.menu ~pack:item_view#set_menu () in
  let (item_new_tab,_) = Menu.label_menuitem ~text:"New tab" ~pack:menu_view#add_item () in
  let (item_split_horizontally,_) = Menu.label_menuitem ~text:"Split horizontally" ~pack:menu_view#add_item () in
  let (item_split_vertically,_) = Menu.label_menuitem ~text:"Split vertically" ~pack:menu_view#add_item () in
  let (item_destroy,_) = Menu.label_menuitem ~text:"Destroy active view" ~pack:menu_view#add_item () in
  let (item_store_layout,_) = Menu.label_menuitem ~text:"Store layout" ~pack:menu_view#add_item () in
  let (item_cycle_tab,_) = Menu.label_menuitem ~text:"Cycle tab" ~pack:menu_view#add_item () in
  let (item_cycle_view,_) = Menu.label_menuitem ~text:"Cycle view" ~pack:menu_view#add_item () in

  let view_menubar = Menu.menubar ~pack:(hboxmenus#pack ~hexpand:0) () in

  let help_menubar = Menu.menubar ~pack:(hboxmenus#pack ~hexpand:1) () in
  let (item_help,_) = Menu.label_menuitem ~text:"Help" ~pack:help_menubar#add_item () in
  let menu_help = Menu.menu ~pack:item_help#set_menu () in
  let (item_about,_) = Menu.label_menuitem ~text:"About" ~pack:menu_help#add_item () in

  let hbox_state : Box.box = Box.hbox ~pack:(vbox#pack ~vexpand:0 ~hexpand:(-1)) () in
  let wl_keystate = Text.label ~classes:["keystate"] (*~width:80 ~has_frame:false*)
    ~pack:(hboxmenus#pack ~hexpand:0) ()
  in
  let () = wl_keystate#set_sensitive false in
  object (self)
    val toplevel = toplevel
    method toplevel = toplevel
    method main = toplevel
    val evbox = evbox
    method evbox = evbox
    val vbox = vbox
    method vbox = vbox
    val hboxmenus = hboxmenus
    method hboxmenus = hboxmenus
    val menubar = menubar
    method menubar = menubar
    val item_file = item_file
    method item_file = item_file
    val menu_file = menu_file
    method menu_file = menu_file
    val item_new_window = item_new_window
    method item_new_window = item_new_window
    val item_open = item_open
    method item_open = item_open
    val item_save = item_save
    method item_save = item_save
    val item_save_as = item_save_as
    method item_save_as = item_save_as
    val item_reload = item_reload
    method item_reload = item_reload
    val item_prefs = item_prefs
    method item_prefs = item_prefs
    val item_log_window = item_log_window
    method item_log_window = item_log_window
    val item_close = item_close
    method item_close = item_close
    val item_edit = item_edit
    method item_edit = item_edit

    val item_new_tab = item_new_tab
    method item_new_tab = item_new_tab

    val item_split_horizontally = item_split_horizontally
    method item_split_horizontally = item_split_horizontally

    val item_split_vertically = item_split_vertically
    method item_split_vertically = item_split_vertically

    val item_destroy = item_destroy
    method item_destroy = item_destroy

    val item_store_layout = item_store_layout
    method item_store_layout = item_store_layout

    val item_cycle_tab = item_cycle_tab
    method item_cycle_tab = item_cycle_tab

    val item_cycle_view = item_cycle_view
    method item_cycle_view = item_cycle_view

    val view_menubar = view_menubar
    method view_menubar = view_menubar

    val item_about = item_about
    method item_about = item_about


    val hbox_state = hbox_state
    method hbox_state = hbox_state
    val wl_keystate = wl_keystate
    method wl_keystate = wl_keystate

  end
class outputs () =
  let toplevel = App.create_window ~resizable:true ~w:700 ~h:150 "Chamo outputs" in
  let notebook = Notebook.hnotebook ~pack:toplevel#set_child () in
  object (self)
    val toplevel = toplevel
    method toplevel = toplevel
    val notebook = notebook
    method notebook = notebook
  end
class outputs_note_tab () =
  let hbox : Box.box = Box.hbox () in
  let wlabel = Text.label ~pack:(hbox#pack ~hexpand:0) () in
  let wb_close, _ = Button.text_button ~text:"✗" ~pack:(hbox#pack ~hexpand:0) ()
  in
  (* FIXME: when available in STK
  let wb_image = GMisc.image ~stock:`CLOSE () in
  let () = wb_close#set_image wb_image#coerce in
  *)
  object (self)
    val hbox = hbox
    method hbox = hbox
    val wlabel = wlabel
    method wlabel = wlabel
    val wb_close = wb_close
    method wb_close = wb_close
    initializer
      let _ = hbox#connect Stk.(Object.Prop_changed Props.selected)
        (fun ~prev ~now -> wlabel#set_selected now)
      in
      ()
  end
